[[!meta title="The evolution of gene expression in the term placenta of viviparous mammals"]]

This is a poster which was presented at the
[IGB Fellows Symposium](http://individualizingmedicineconference.mayo.edu/)
and the [KnowEng 2016 EAC meeting](http://www.knoweng.org/content/events-0).

* [Poster code in git](https://github.com/uiuc-cgm/placenta-viviparous)
* [PDF of poster](http://www.donarmstrong.com/ld/pv2016/placenta_viviparous_poster.pdf)
* [Slides of a talk on this work](http://www.donarmstrong.com/ld/pv2016/placenta_viviparous_presentation.pdf)

This represents a work which is currently under consideration for publication.

[[!tag posters biology]]
