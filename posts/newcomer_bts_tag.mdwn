[[!meta title="Adding a newcomer (⎈) tag to the BTS"]]

Some of you may already be aware of the
[gift tag](https://bugs.debian.org/cgi-bin/pkgreport.cgi?tag=gift;users=debian-qa@lists.debian.org)
which has been used for a while to indicate bugs which are suitable
for new contributors to use as an entry point to working on specific
packages. Unfortunately, some of us (including me!) were unaware that
this tag even existed.

Luckily, Lucas Nussbaum clued me in to the existence of this tag, and
after a brief bike-shed-naming thread, and some
[voting using pocket_devotee](http://git.donarmstrong.com/?p=bin.git;a=blob_plain;f=pocket_devotee;hb=fbd5071cf1eb41f9fc62325a6142dd10a41f1717)
we decided to name the new tag newcomer, and I have now added this tag
to the BTS documentation, and tagged all of the bugs which were user
tagged "gift" with this tag.

If you have bugs in your package which you think are ideal for new
contributors to Debian (or your package) to fix, please tag them
newcomer. If you're getting started in Debian, and working on bugs to
fix, please
[search for the newcomer tag](http://bugs.debian.org/cgi-bin/pkgreport.cgi?tag=newcomer),
grab the helm, and contribute to Debian.

[[!tag debian tech debbugs]]
