[[!meta title="Identifying the Tissue of Origin of Extracellular Vesicles Using RNA Expression Signatures"]]

This is a poster which was presented at the
[Individualizing Medicine 2015 conference at the Mayo Clinic](http://individualizingmedicineconference.mayo.edu/).

* [Poster code in git](https://github.com/uiuc-cgm/exosome_markers_per_med_sep_2015)
* [PDF of poster](http://www.donarmstrong.com/ld/em2015/exosome_markers_per_med_sep_2015_poster.pdf)
* [Slides of a talk on this work](http://www.donarmstrong.com/ld/em2015/exosome_markers_per_med_sep_2015_slides.pdf)
* [Gene specific markers table](http://www.donarmstrong.com/ld/em2015/exosome_markers_per_med_sep_2015_gene_markers_table.txt)

[[!tag posters biology]]
