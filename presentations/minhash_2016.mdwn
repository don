[[!meta title="Mash: fast MinHash based k-mer sequence comparison"]]

This is a paper talk which I presented on June 23rd at HPC Bio at UIUC.


* [Code for slides in git](http://git.donarmstrong.com/mash_minhash_presentation.git)
* [PDF of slides](http://www.donarmstrong.com/ld/minhash2016/hpcbio_mash_minhash_jun_2016.pdf)

[[!tag presentations biology]]
