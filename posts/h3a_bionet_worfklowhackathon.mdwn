[[!meta title="H3ABioNet Hackathon (Workflows)"]]

I'm in Pretoria, South Africa at the
[H3ABioNet](http://h3abionet.org/)
[hackathon](http://h3abionet.org/17-h3abionet-courses/h3abionet-courses-upcoming/266-h3abionet-cloud-computing-hackathon)
which is developing workflows for Illumina chip genotyping,
imputation, 16S rRNA sequencing, and population structure/association
testing. Currently, I'm working with the imputation stream and we're
using [Nextflow](https://www.nextflow.io/) to deploy an
[IMPUTE](https://mathgen.stats.ox.ac.uk/impute/impute_v2.html)-based
imputation workflow with Docker and
[NCSA's openstack-based cloud (Nebula)](https://wiki.ncsa.illinois.edu/display/NEBULA/Nebula+Home)
underneath.

The OpenStack command line clients (`nova` and `cinder`) seem to be
pretty usable to
[automate bringing up a fleet of VMs](https://github.com/h3abionet/chipimputation/blob/master/openstack/generate_openstack)
and the cloud-init package which is present in the images makes
[configuring the images pretty simple](https://github.com/h3abionet/chipimputation/tree/master/openstack).

Now if I just knew of a better shared object store which was supported
by Nextflow in OpenStack besides mounting an NFS share, things would
be better.

You can follow our progress in our git repo:
[https://github.com/h3abionet/chipimputation]


[[!tag debian tech biology workflows]]
