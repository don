[[!meta title="Core Transcriptome of Mammalian Placentas"]]

Our
[paper which describes the components of the placenta transcriptome which are conserved among all placental mammals](http://www.sciencedirect.com/science/article/pii/S0143400417302369) in
*Placenta* just came out today. More importantly than the results and
the text of the paper, though, is the fact that all of the code and
results of this paper, from the very first work I did two years ago to
its publication today is present in git, and (in theory) reproducible.

You can see where our paper was rejected from
*[Genome Biology](https://github.com/uiuc-cgm/placenta-viviparous/tree/genome_biology_submission_4202016)*
and
*[Genes and development](https://github.com/uiuc-cgm/placenta-viviparous/tree/genes_dev_submission)* and 
[radically refocused before submission to
*Placenta*](https://github.com/uiuc-cgm/placenta-viviparous/compare/genes_dev_submission...master#diff-dfaf04b448930bc40fe8b2907f2c7223).
But more importantly, you can know where every single result which is
mentioned in the paper came from, the precise code to generate it, and
how we came to the final paper which was published. [And you've also
got all of the hooks to branch off from our analysis to do your own
analysis based on our data!]

This is what open, reproducible science should look like.

[[!tag biology placenta openscience]]
