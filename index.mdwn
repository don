[[!meta description="Don Armstrong's blog and web pages. Don Armstrong is a Bioinformatician and Debian Developer currently at the University of Illinois at Urbana-Champaign"]]

[[!inline pages="./posts/* and !*/Discussion" show="10"
actions=yes rootpage="posts"]]

[[!if test="enabled(sidebar)" then="""
[[!sidebar]]
""" else="""
[[!inline pages=sidebar raw=yes]]
"""]]


This blog is powered by [ikiwiki](http://ikiwiki.info).
